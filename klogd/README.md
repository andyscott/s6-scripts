Based on the [example](https://git.skarnet.org/cgi-bin/cgit.cgi/s6/tree/examples/klogd-linux) provided by s6,
Copyright (c) 2011 Laurent Bercot [ISC License](https://git.skarnet.org/cgi-bin/cgit.cgi/s6/tree/COPYING)
